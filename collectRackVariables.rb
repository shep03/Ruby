# Description: This Method will collect the variables parsed from the Rack Dialog
#
##
require 'RackClass'

$evm.log(:debug, "New Rack Initiated")

# Get the Data Centre Value

instance = rack.new()

dc = $evm.root['DC']
ru = $evm.root['Number_of_RU']
cust = $evm.root['Customer_Name']

instance.set(ru, dc, cust)
if instace.isavailable instance.provision else throw_an_error("No Space")


exit MIQ_OK
