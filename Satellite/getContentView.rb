#!/usr/bin/env ruby
#
require 'io/console'
require 'rest-client'
require 'json'

## Edit he URL to point to your Satellite server
url = 'https://foreman.example.net'
katello_url = "#{url}/katello/api"

## This currently will prompt you for a password when you run it. Feel free to hardcode
## a password by uncommenting the below and substituting your own password.
#password = YOURPASSWORD

puts "Enter the password:"

## If you choose to hard code the password. Be sure to comment this line out.
password = STDIN.noecho(&:gets).chomp

## Using the default admin user. Again, change this to your own.
$username = 'admin'
$password = password

## Specifying the Organisation and the Environment we want to publish to.
org_name = 'Default Organization'
environments = 'Library'


# Performs a GET using the passed URL location
def get_json(location)
  response = RestClient::Request.new(
    :method => :get,
    :url => location,
    :user => $username,
    :password => $password,
    :verify_ssl => false,
    :headers => { :accept => :json,
    :content_type => :json }
  ).execute
  JSON.parse(response.to_str)
end

## You can use these two lines to find the number of the Content View you wish to publish

#cv_json = get_json("#{katello_url}/content_views")
#puts JSON.pretty_generate(cv_json)

## This function will publish the specified Content View
def publish_cv(location)
 response = RestClient::Request.new(
    :method => :post,
    :url => location,
    :user => $username,
    :password => $password,
    :verify_ssl => false,
    :headers => { :accept => :json,
    :content_type => :json }
 ).execute
 JSON.parse(response.to_str)
end

## I have specified the Content View with ID 2 in the command below. This line will do the
## publishing on the specified Content View by calling the above function

publish = publish_cv("#{katello_url}/content_views/2/publish")
puts JSON.pretty_generate(publish)
