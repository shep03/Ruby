#!/usr/bin/env ruby
#
## curl -s 'https://places.nbnco.net.au/places/v2/details/LOC000030706211' -H 'X-NBN-Breadcrumb-Id: 1538202437980-418' -H 'Accept: */*' -H 'Referer: https://www.nbnco.com.au/connect-home-or-business/check-your-address.html'

require 'io/console'
require 'rest-client'
require 'json'

id = "LOC000030706211" 
url = "https://places.nbnco.net.au/places/v2/details/#{id}"

def get_json(location)
  response = RestClient::Request.new(
    :method => :get,
    :url => location,
    :verify_ssl => true,
    :headers => { :accept => :"*/*",
    :content_type => :json,
    :"X-NBN-Breadcrumb-Id" => :"1538202437980-418",
    :"Referer" => "https://www.nbnco.com.au/connect-home-or-business/check-your-address.html" }
  ).execute
  return(JSON.parse(response.to_str))
end

def format_message(json)
  res = json
  approx = res['addressDetail']['rfsMessage']
  tech = res['addressDetail']['techType']
  status = res['addressDetail']['serviceStatus']
  message = "The NBN will arrive between #{approx}\n Using the technology: #{tech}\n And it's current status is: #{status}\n"
  return(message)
end

puts(format_message(get_json(url)))

## If we need the full output, uncomment this fucker.
#puts JSON.pretty_generate(res)
